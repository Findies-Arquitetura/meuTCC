package br.ifrn.meutcc.persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import br.ifrn.meutcc.modelo.Orientador;

public class OrientadorDAO {
	private static final String MySQLDriver = "com.mysql.jdbc.Driver";
	private Connection conn = null;

	public Orientador getOrientadorPorTema(int idTema) {
		verificaConexao();
		if (conn != null) {
			try {
				Statement stListaTema = conn.createStatement();
				ResultSet rsOrientador = stListaTema.executeQuery("select * from Orientador where Tema_id =  " + idTema);
				Orientador orientador = null;
				if (rsOrientador.next()) {
					orientador = new Orientador();
					orientador.setId(rsOrientador.getInt("IdOrientador"));
					orientador.setNome(rsOrientador.getString("Nome"));
					orientador.setEmail(rsOrientador.getString("Email"));
					orientador.setTelefone(rsOrientador.getString("Telefone"));

				}
				return orientador;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private void verificaConexao() {
		String url = "jdbc:mysql://localhost/meutcc1", nome = "root", senha = "meubanco10";
		if (conn != null) {
			return;
		}
		try {
			Class.forName(MySQLDriver);
			conn = DriverManager.getConnection(url, nome, senha);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
