package br.ifrn.meutcc.persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AlunoDAO {
	private static final String MySQLDriver = "com.mysql.jdbc.Driver";
	private Connection conn = null;

	public int getCountCandidatos(int idTema) {
		verificaConexao();
		if (conn != null) {
			try {
				Statement stQuantidadeAluno = conn.createStatement();
				ResultSet rsQuantidade = stQuantidadeAluno
						.executeQuery("select count(Tema_id) from Aluno where Tema_id = " + idTema);
				int quantidade = -1;
				while (rsQuantidade.next()) {
					quantidade = rsQuantidade.getInt("count(Tema_id)");
				}
				return quantidade;

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return -1;
	}

	public boolean addCandidato(int idTema, int idAluno) {
		verificaConexao();
		if (conn != null) {
			try {
				Statement statementAlunoTema = conn.createStatement();
				statementAlunoTema
						.executeUpdate("update Aluno set Tema_id=" + idTema + " where idAluno=" + idAluno);
				return true;
			} catch (SQLException e) {
				e.printStackTrace();

			}

		}
		return false;
	}

	private void verificaConexao() {
		String url = "jdbc:mysql://localhost/meutcc1", nome = "root", senha = "meubanco10";
		if (conn != null) {
			return;
		}
		try {
			Class.forName(MySQLDriver);
			conn = DriverManager.getConnection(url, nome, senha);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
