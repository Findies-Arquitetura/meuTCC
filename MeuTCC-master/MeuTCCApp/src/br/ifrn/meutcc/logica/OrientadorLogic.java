package br.ifrn.meutcc.logica;

import br.ifrn.meutcc.modelo.Orientador;
import br.ifrn.meutcc.persistencia.OrientadorDAO;

public class OrientadorLogic {
	private Orientador orientador;
	private OrientadorDAO orientadorDao = new OrientadorDAO();
	
	public OrientadorLogic(){
		orientador = new Orientador();
	}
	
	public Orientador getOrientadorPorTema(int idTema){
		return orientadorDao.getOrientadorPorTema(idTema);
	}
	
}
