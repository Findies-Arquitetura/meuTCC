package br.ifrn.meutcc.logica;

import br.ifrn.meutcc.modelo.Aluno;
import br.ifrn.meutcc.persistencia.AlunoDAO;

public class AlunoLogic {
	private Aluno aluno;
	private AlunoDAO alunoDAO = new AlunoDAO();

	public AlunoLogic() {
		aluno = new Aluno();
	}

	public Aluno getAluno() {
		return aluno;
	}
	public boolean addCandidato(int idTema,int  idAluno){
		return alunoDAO.addCandidato(idTema, idAluno); 
	}
}
