package br.ifrn.meutcc.visao;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.ifrn.meutcc.logica.AlunoLogic;

/**
 * Servlet implementation class Candidatura
 */
@WebServlet("/Candidatura")
public class Candidatura extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public Candidatura() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int idTema = Integer.parseInt(request.getParameter("idTema"));
		int idAluno = Integer.parseInt(request.getParameter("idAluno"));
		AlunoLogic alunoLogic = new AlunoLogic();
		boolean resultadoCadastro  = alunoLogic.addCandidato(idTema,idAluno);
		
		//System.out.println(resultadoCadastro + " --- ");
		
		request.setAttribute("idTema", idTema);
		request.setAttribute("idAluno", idAluno);
		request.setAttribute("resultado", resultadoCadastro);
		request.getRequestDispatcher("candidatura.jsp").forward(request, response);
	
	}


}
