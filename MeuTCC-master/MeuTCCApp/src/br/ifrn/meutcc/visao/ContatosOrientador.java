package br.ifrn.meutcc.visao;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.ifrn.meutcc.logica.OrientadorLogic;
import br.ifrn.meutcc.modelo.Orientador;

/**
 * Servlet implementation class ContatosOrientador
 */
@WebServlet("/ContatosOrientador")
public class ContatosOrientador extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ContatosOrientador() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
			int idTema = Integer.parseInt(request.getParameter("idTema"));
			OrientadorLogic orientadorLogic = new OrientadorLogic();
			Orientador orientador = orientadorLogic.getOrientadorPorTema(idTema);
			
		
			request.setAttribute("orientador", orientador);
		    request.getRequestDispatcher("contatosOrientador.jsp").forward(request, response);
	}

}
