package br.ifrn.meutcc.visao;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.ifrn.meutcc.logica.AlunoLogic;

/**
 * Servlet implementation class ListCandidatos
 */
@WebServlet("/quantidadeCandidatos")
public class QuantidadeAluno extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public QuantidadeAluno() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("id");
		//System.out.println(id);
		int idTema = -1;
		try {
			idTema = Integer.parseInt(id);

		} catch (NumberFormatException nfex) {
			nfex.printStackTrace();

		}

		AlunoLogic alunoLogic = new AlunoLogic();
		int quantidadeAlunos = alunoLogic.getAluno().countCandidatos(idTema);
		int idTeema = Integer.parseInt(id);
		request.setAttribute("quantidadeAlunos", quantidadeAlunos);
		request.setAttribute("idTema", idTeema);
		request.getRequestDispatcher("quantidadeAlunos.jsp").forward(request, response);
	}

}
