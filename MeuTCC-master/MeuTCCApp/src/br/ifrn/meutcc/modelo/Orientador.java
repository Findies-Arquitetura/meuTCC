package br.ifrn.meutcc.modelo;

import br.ifrn.meutcc.persistencia.OrientadorDAO;

public class Orientador {
	private int id;
	private String nome;
	private String email;
	private String telefone;
	private OrientadorDAO orientadorDAO;

	public Orientador() {
		orientadorDAO = new OrientadorDAO();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

}
