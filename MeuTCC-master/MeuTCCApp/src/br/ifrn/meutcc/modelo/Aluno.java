package br.ifrn.meutcc.modelo;

import br.ifrn.meutcc.persistencia.AlunoDAO;

public class Aluno {
	private int id;
	private String nome;
	private String matricula;
	private AlunoDAO alunoDao;
	
	public Aluno(){
		alunoDao = new AlunoDAO();
	}

	private Tema tema;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public Tema getTema() {
		return tema;
	}

	public void setTema(Tema tema) {
		this.tema = tema;
	}

	public int countCandidatos(int idTema) {
		return alunoDao.getCountCandidatos(idTema);
	}

}
