<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.List,br.ifrn.meutcc.modelo.Tema"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Meu TCC</title>
</head>
<body>
	<h1>Listagem dos Temas Disponíveis para TCC</h1>
	<h3>Candidatura</h3>

	<%
		int idTema = (int) request.getAttribute("idTema");
		int idAluno = (int) request.getAttribute("idAluno");
		boolean resultado = (boolean) request.getAttribute("resultado");
		if(resultado){
			out.print("Cadastro realizado com sucesso !");
			out.print("<li><a href=\"/MeuTCC/ContatosOrientador?idTema=" 
					+ idTema + "\">Contatos do orientador </a></li>");
		}else {
			out.print("Erro");
		}
	%>
</body>
</html>